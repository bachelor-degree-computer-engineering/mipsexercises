                    .data 
vettore:            .byte 14, 16, 18, 134, 24, 22, 23, 149, 140, 141, 145, 146 
vettoreCompresso:   .space 12 
INTERVALLO_QUANT = 10 
 
                    .text  
                    .globl main  
                    .ent main 
main:               subu $sp, $sp, 4  
                    sw $ra, ($sp)  
                    la $a0, vettore    
                    li $a1, 12  
                    la $a2, vettoreCompresso  
                    jal comprimi    
                    lw $ra, ($sp)  
                    addu $sp, $sp, 4  
                    jr $ra  
                    .end main 
 
quantizza:          divu $t0, $a0, INTERVALLO_QUANT  
                    mulou $v0, $t0, INTERVALLO_QUANT  
                    jr $ra

comprimi:           addi $sp, $sp, -4
                    sw $ra, ($sp)
                    # $s0 -> vettore , $s1 -> vettoreCompresso
                    # $s2 -> indice ciclo, $s3 -> valore precedente
                    # $s4 numero di elementi
                    #salvo i valori nello stack
                    addi $sp, $sp, -4
                    sw $s0, ($sp)
                    addi $sp, $sp, -4
                    sw $s1, ($sp)
                    addi $sp, $sp, -4
                    sw $s2, ($sp)
                    addi $sp, $sp, -4
                    sw $s3, ($sp)
                    addi $sp, $sp, -4
                    sw $s4, ($sp)

                    move $s0, $a0
                    move $s1, $a2
                    and $s2, $0, $0
                    and $s4, $0, $0

loop:               beq $s2, $a1, exit
                    lb $a0, ($s0)      # lettura valore
                    jal quantizza
                    beqz $s3, newValue
                    beq $s3, $v0, done # salto se il valore è stato già inserito

newValue:           move $s3, $v0      # $s3 nuovo valore precedente
                    sb $s3, ($s1)
                    addi $s1, $s1, 1
                    addi $s4, $s4, 1

done:               addi $s0, $s0, 1
                    addi $s2, $s2, 1
                    j loop
exit:               
                    move $v0, $s4      # valore di ritorno.
                    lw $s4, ($sp)
                    addi $sp, $sp, 4
                    lw $s3, ($sp)
                    addi $sp, $sp, 4
                    lw $s2, ($sp)
                    addi $sp, $sp, 4
                    lw $s1, ($sp)
                    addi $sp, $sp, 4
                    lw $s0, ($sp)
                    addi $sp, $sp, 4
                    lw $ra, ($sp)
                    addi $sp, $sp, 4
                    jr $ra

# Algoritmo
# La procedura scandisce ogni elemento del vettore. Ad ogni iterazione 
# del ciclo, richiama la procedura quantizza (il cui codice è fornito 
# sotto) per ottenere il valore quantizzato dell’elemento corrente. 
# Dopo aver ottenuto il valore quantizzato, la procedura comprimi lo 
# confronta con l’ultimo elemento inserito nel vettore compresso. 
# Il nuovo valore quantizzato è inserito nel vettore compresso solo 
# se diverso. 