                    .data
RIGHE = 4
COLONNE = 6
matrice:            .byte 113,  10,  95,  20,  60,  10
                    .byte  51,  26, 120,  30,  56,  13
                    .byte 102, 171,  21,  11,  17,  22
                    .byte 204, 100, 230,  16,  76,  34
                    .text
                    .globl main
                    .ent main

main:               addi $sp, $sp, -4
                    sw $ra, ($sp)
                    la $a0, matrice
                    li $a1, RIGHE
                    li $a2, COLONNE
                    jal contaColonne

                    move $a0, $v0
                    li $v0, 1
                    syscall

                    lw $ra, ($sp)
                    addi $sp, $sp, 4
                    jr $ra
                    .end main

# ContaColonne è una procedura che conta il numero di colonne in cui il numero
# di elementi pari è maggiore del numero di elementi dispari. Riceve tramite
# $a0 l'indirizzo della matrice e $a1, $a2 le dimensioni. Restituisce in $v0
# il risultato.

contaColonne:       # $t0 -> $a0, $t1 -> indice esterno, $t2 -> indice interno.
                    # $t3 conterrà riferimento alla colonna.
                    # $v0 conta.
                    # $t5 conta i pari, $t6 i dispari
                    move $t0, $a0
                    li $t1, 0
                    li $t2, 0
                    li $v0, 0
loopExt:            beq $t1, $a2, exitExt
                    move $t3, $t0
                    li $t5, 0
                    li $t6, 0
loopInt:            beq $t2, $a1, exitInt
                    lb $t4, ($t0)
                    andi $t4, $t4, 1
                    beq $t4, $0, pari
                    addi $t6, $t6, 1
                    j done
pari:               addi $t5, $t5, 1
done:               add $t0, $t0, $a2    #viaggio per colonne
                    addi $t2, $t2, 1
                    j loopInt
exitInt:            bgt $t6, $t5, noAdd
                    addi $v0, $v0, 1
noAdd:              move $t0, $t3
                    addi $t0, $t0, 1
                    addi $t1, $t1, 1
                    j loopExt
exitExt:
                    jr $ra