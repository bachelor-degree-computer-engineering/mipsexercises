DIM = 3 
DIM_RIGA = DIM * 4 

                        .data 
mat1:                   .word 4, -45, 15565,   
                        .word 6458, 4531, 124,
                        .word -548, 2124, 31000 
mat2:                   .word 6, -5421, -547,   
                        .word -99, 4531, 1456,   
                        .word 4592, 118, 31999 
indice:                 .word 2
vet_out:                .space  DIM_RIGA 
 
                        .text 
                        .globl main 
                        .ent main 
main:  
                        subu $sp, $sp, 4 
                        sw $ra, ($sp)                                   
                        la $a0, mat1 
                        la $a1, mat2 
                        la $a2, vet_out 
                        li $a3, DIM 
                        subu $sp, $sp, 4 
                        lw $t0, indice 
                        sw  $t0, ($sp)                                    
                        jal ProceduraVariazione 
                        addu $sp, $sp, 4                    
                        lw  $ra, ($sp) 
                        addu $sp, $sp, 4 
                        jr $ra 
                        .end main 

ProceduraVariazione:    # $t1 -> matrice1, $t2 -> matrice 2
                        # $t3 -> vet_out, $t0 -> i nel ciclo
                        # $t4 -> indice passato tramite stack

                        and $t0, $0, $0
                        move $t1, $a0
                        move $t2, $a1
                        move $t3, $a2
                        lw $t4, ($sp)
loop:                   beq $t0, $a3, exit
                        # $t5 =  matrice1($t4, $t0), 
                        # $t7 conterrà l'indirizzo
                        mul $t7, $t4, $a3
                        sll $t7, $t7, 2
                        sll $t8, $t0, 2
                        add $t7, $t7, $t8
                        add $t7, $t1, $t7

                        lw $t5, ($t7)

                        # $t6 = matrice2($t0, $t4)
                        # $t7 conterrà l'indirizzo
                        mul $t7, $t0, $a3
                        sll $t7, $t7, 2
                        sll $t8, $t4, 2
                        add $t7, $t7, $t8
                        add $t7, $t2, $t7

                        lw $t6, ($t7)

                        #calcolo la variazione in $t7
                        sub $t7, $t6, $t5
                        mul $t7, $t7, 100
                        div $t7, $t7, $t5
                        # salvo il valore nel vettore out
                        sw $t7, ($t3)

                        addi $t3, $t3, 4
                        addi $t0, $t0, 1
                        j loop
exit:
                        jr $ra
                        .end ProceduraVariazione