
                .data
DIM = 4
DIMREND = 4*DIM  
prezzo_acq:     .word 190, 68, 71, 84
prezzo_vend:    .word 199, 40, 81, 90
dividendo:      .word 2, 5, 0, 1
rendimento:     .space DIMREND
                .text
                .globl main
                .ent main
            

main:           addi $sp, $sp, -4
                sw $ra, ($sp)
                # La procedura riceve in
                # $a0 -> vettore prezzi acquisto
                # $a1 -> vettore prezzo vendita
                # $a2 -> vettore dividendo
                # $a3 -> vettore rendimento
                # DIM tramite stack(non richiesto dal problema)
                la $a0, prezzo_acq
                la $a1, prezzo_vend
                la $a2, dividendo
                la $a3, rendimento
                li $t0, DIM
                addi $sp, $sp, -4
                sw $t0, ($sp)

                jal calcola

                addi $sp, $sp, 4 #svuoto lo stack da DIM.
                lw $ra, ($sp)
                addi $sp, $sp, 4
                jr $ra
                .end main

calcola:        #avrò bisogno di 
                #t0 -> $a0, $t1 -> $a1, $t2 -> a2, $t3 -> $a3
                #$t4 -> indice, $t5 -> DIM
                # $s0 -> $t0, $s1 -> t1, $s2 -> $t2, 

                
                addi $sp, $sp, -4
                sw $s0, ($sp)
                addi $sp, $sp, -4
                sw $s1, ($sp)
                addi $sp, $sp, -4
                sw $s2, ($sp)

                move $t0, $a0
                move $t1, $a1
                move $t2, $a2
                move $t3, $a3
                li $t4, 0
loop:           beq $t4, DIM, exit

                lw $s0, ($t0)
                lw $s1, ($t1)
                lw $s2, ($t2)

                sub $t6, $s1, $s0
                add $t6, $t6, $s2
                div $t6, $t6, $s0

                sw $t6, ($t3)

                addi $t0, $t0, 4
                addi $t1, $t1, 4
                addi $t2, $t2, 4
                addi $t3, $t3, 4
                addi $t4, $t4, 1
                j loop
exit:           
                lw $s2, ($sp)
                addi $sp, $sp, 4
                lw $s1, ($sp)
                addi $sp, $sp, 4
                lw $s0, ($sp)
                addi $sp, $sp, 4

                jr $ra