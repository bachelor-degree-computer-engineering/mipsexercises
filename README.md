# MIPSexercises

This repository contains my exercise in assembly MIPS for the CE exam in polito. Some of them are real exam, some other are algorithms.

## List of exercises
- Transpose of a matrix;
- Reverse Polish function;
- longest increasing subsequence(contiguos);
- Variation
- compression of a vector

## Accessing exercises
Some exercises are in a beta version: this means that I'm not so sure of that codes, so I push them to a branch with a specific name.