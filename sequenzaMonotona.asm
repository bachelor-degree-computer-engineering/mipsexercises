                .data 
vet:            .word  15, 64, 9, 2, 4, 5, 9, 1, 294, 52, -4, 5 
                .text                  
                .globl main               
                .ent main 
main:            subu $sp, $sp, 4                  
                 sw $ra, 0($sp)                     
                 la $a0, vet         # indirizzo di vet                  
                 li $a1, 12          # dimensione di vet                  
                 jal monotono 
                 move $s0, $v0
                 lw $ra, 0($sp)                  
                 addiu $sp, $sp, 4    

                 jr $ra
                 .end main

monotono:       # $t0 contiene l'indirizzo del vettore.
                # $t1 è un indice.
                # $t2 contiene l'elemento precedente, ed è inizializzato all'intero più piccolo.
                # $t3 contiene l'elemento corrente.
                # $t4 contiene l'indice della sequenza monotona più lunga.
                # $t5 contiene la lunghezza.
                move $t0, $a0
                and $t4, $0, $0
                and $t1, $0, $0
                li $t2, -1
                li $v0, 0
                li $v1, 0
loop:           beq $t1, $a1, exit
                lw $t3, ($t0)
                bgt $t3, $t2, isMonotona
                # se sono qui ho trovato un elemento non monotono
                # controllo se la sequenza appena vista è più lunga
                blt $v1, $t5, no
                move $t4, $v0
                move $t5, $v1
                move $v0, $t1
                addi $v0, $v0, 1
no:             li $v1, 0
                j done
isMonotona:     addi $v1, $v1, 1
done:           move $t2, $t3 # preparo l'elemento precedente per il prossimo ciclo
                addi $t0, $t0, 4
                addi $t1, $t1, 1
                j loop
exit:           move $v0, $t4
                move $v1, $t5
                jr $ra
                .end monotono

#  Algoritmo: Inizialmente, il vettore contenent la sequenza monotona è il primo elemento
#  e la sua lunghezza è 1. Si cicla sul vettore, verificando che l'elemento attuale è maggiore
#  del precedente. Se non lo è, si resetta, e si considera il corrente come nuovo inizio della
#  sequenza monotona.