                .data
espressione:    .word 18, 25, 10, 7, -2, -3, -1, 13, -2 
tabella:        .word somma, sottrazione, moltiplicazione, divisione  
                .text
               .globl main  
                .ent main 
main:           subu $sp, $sp, 4
                sw $ra, ($sp)
                la $a0, espressione  
                li $a1, 9  

                jal calcolaPolaccaInversa
                
                lw $ra, ($sp)  
                addu $sp, $sp, 4  
                jr $ra  
                .end main 
 
eseguiOperazione:  
                subu $t0, $zero, $a0  
                subu $t0, $t0, 1  
                sll $t0, $t0, 2  
                lw $t1, tabella($t0)  
                jr $t1 

somma:          addu $v0, $a1, $a2  
                b fine 

sottrazione:
                subu $v0, $a1, $a2  
                b fine 
moltiplicazione:
                mulou $v0, $a1, $a2  
                b fine 
            
divisione:      divu $v0, $a1, $a2 
                b fine 
fine:           jr $ra 


calcolaPolaccaInversa:
                addi $sp, $sp, -4
                sw $ra, ($sp)
                move $t0, $a0   #$t0 contiene l'indirizzo dell'espressione
                move $t1, $a1   #$t1 contiene la lunghezza del'espressione
                and $t2, $0, $0 #$t2 è l'indice nel ciclo
                li $t3, 0x8000  #maschera per il primo bit
loop:           beq $t2, $t1, exit
                lw $t4, ($t0)
                and $t5, $t3, $t4
                beq $t5, $0, isOperand
                # devo fare un operazione: pop dei due operandi
                lw $a2, ($sp)
                addi $sp, $sp, 4
                lw $a1, ($sp)
                addi $sp, $sp, 4
                move $a0, $t4    # operatore

                # procedura non leaf, salvo nello stack i registi $t0, $t1

                addi $sp, $sp, -4
                sw $t0, ($sp)
                addi $sp, $sp, -4
                sw $t1, ($sp)

                jal eseguiOperazione

                lw $t1, ($sp)
                addi $sp, $sp, 4
                lw $t0, ($sp)
                addi $sp, $sp, 4
                # recupero i valori dei registri

                addi $sp, $sp, -4
                sw $v0, ($sp)    #push del risultato nello stack
                j done
isOperand:      add $sp, $sp, -4
                sw $t4, ($sp)    # push nello stack di $t4
done:           addi $t0, $t0, 4
                addi $t2, $t2, 1
                j loop
exit:           lw $v0, ($sp)
                addi $sp, $sp, 4
                lw $ra, ($sp)
                addi $sp, $sp, 4
                jr $ra

# Algoritmo: scandisco l'array, se è un operando ne faccio il push nello stack
# altrimenti se è un operatore fa il pop di due elementi e chiama eseguiOperazione
# il valore è inserito in cima allo stack. 
# calcolaPolacca invera come prima operazione salva $ra nello stack.

#EVOLUZIONE DELLO STACK
#    
#    -> $ra