                 DIM = 4                  
                .data 
matrice:        .word 126, -988, 65, 52
                .word 7, 0, 2, 643 
                .word 66, 532, 43, 9254        
                .word 5, -51, 4352, -452 

                .text 
                .globl main                  
                .ent main 
 
main:           subu $sp, $sp, 4
                sw $ra, ($sp)                  
                la $a0, matrice                  
                li $a1, DIM                  
                jal calcolaTrasp                  
                lw $ra, ($sp)                  
                addiu $sp, $sp, 4                  
                jr $ra

calcolaTrasp:   #a0 contiene l'indice della matrice e tiene conto della diagonale
                # $t0 ciclo esterno
                # $t1 ciclo interno (da $t0+1 a DIM)
                # $t2, $t3 usati per generare gli indirizzi a partire dagli indici
                # $t4 contiene l'offset a cui saltare

                li $t0, 0
                li $t1, 0
                addi $t4, $a1, 1
loopExt:        beq $t0, $a1, exitExt
                addi $t1, $t0, 1
loopInt:        beq $t1, $a1, exitInt
                # accedo all'elemento di indice $t0, $t1 e faccio swap
                mul $t2, $t0, $a1
                sll $t2, $t2, 2
                sll $t3, $t1, 2
                add $t2, $t2, $t3 
                add $t2, $t2, $a0 # indirizzo elemento $t0, $t1

                mul $t3, $t1, $a1
                sll $t3, $t3, 2
                sll $t4, $t0, 2
                add $t3, $t3, $t4
                add $t3, $a0, $t3 # indirizzo elemento $t1, $t0

                # $t5 $t6 registri di supporto
                lw $t5, ($t2)
                lw $t6, ($t3)
                sw $t5, ($t3)
                sw $t6, ($t2)  
                # eseguo lo swap e vado avanti

                addi $t1, $t1, 1
                j loopInt
exitInt:        add $t0, $t0, 1
                j loopExt
exitExt:
                jr $ra

# la procedura fa uno swap delle varibili rispetto alla diagonale
# Per ogni riga, se non è una diagonale, fai uno swap con l'elemento 
# corrispondente. Ad ogni iterazione esterna vado avanti di DIM*4 + 4.
# e successivamente ciclo fino alla fine. se un elemento ha indice (i, j), dopo avrà indice (j, i)